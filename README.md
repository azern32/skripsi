# Skripsi

## Melihat file skripsi
Cukup dengan memilih `Skripsi.docx` lalu download filenya.

## Melihat Progress
Setiap file memiliki waktu kapan terakhir kali file mendapat perubahan. Cukup dengan melihat "Last updated" pada daftar file.


# Feedback dan Progress keseluruhan
## Issues
### List Issues
List Issues digunakan untuk mendaftarkan semua feedback yang diterima dari dosen. Bila feedback telah dilakukan, maka issue akan berganti status dari `open` menjadi `closed`

### Milestones
Progress skripsi keseluruhan dapat dilihat pada daftar Milestones. Masing-masing sub-bab berfungsi sebagai indikator persentase penyelesaian masing-masing bab. Sub-bab yang tidak menerima feedback dan diterima oleh dosen akan diberi tanda centang. Bila keseluruhan sub-bab dalam suatu bab telah dicentang semua, maka Milestones telah dicapai.


# Target Variable
`Persepsi investasi yg sudah dilakukan oleh content creator`

`Pengalaman berdonasi`

# Apparent Progress
## Bab 1 
-[] Latar Belakang Masalah
-[] Rumusan Masalah

## Bab 2
-[] Tinjauan Pustaka
-[] Kerangka Konseptual

## Bab 3
-[] Jenis Penelitian
-[] desain Penelitian
-[] Variabel Penelitian
-[] Definis Operasional Variabel Penelitian
-[] Populasi dan Sampel
-[] Teknik Pengumpulan Data
-[] Teknik Analisis Data
-[] Prosedur Kerja

## Bab 4
-[] Hasil Penelitian
-[] Pembahasan
 
## Bab 5
-[] Kesimpulan
-[] Saran

# Things to do
- ganti soal intensi (planned behavior) ke pengalaman berdonasi (karena sudah tertarget dari awal dan jelas)
- pertanyaan terkait pengalaman berdonasi (open question)
	- soal besar-frekuensi
	- adakah budget tertentu
	- besar pendapatan
	- tanggungan yang dimiliki
	- living condition
- coping mechanism (?)
-----------------
- berita soal vtuber
- fakta soal vtuber ini
- penjelasan soal pengalaman berdonasi (nda usah cari teori, pengalamannya yang sudah diwawancarai saja)
- 